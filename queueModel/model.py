import math
from numpy import exp, random
import time
from IPython.display import clear_output


def distribution(dtype: str, args):
    if dtype == "static":
        distr = args[0]
    
    elif dtype == "uniform":
        distr = random.uniform(*args)
    
    elif dtype == "poisson":
        distr = random.poisson(args[0])
    
    elif dtype == "normal":
        distr = random.normal(*args)
    
    return distr


class QueueItem:
    def __init__(self, id: int):
        self.id = id
        self.waitingTime = 0
        self.workingTime = 0
        self.status = "idle"
    
    def update(self, timediff):
        self.waitingTime += timediff
        

class Agent:
    def __init__(
        self, 
        id: int, 
        measure: str = "sec"
    ):
        self.id = id
        self.current = None
        self.status = "idle"
        self.workingTime = 0
        self.measure = measure
    
    def perform(self, item: QueueItem, workingTime: float):
        self.current = item
        self.workingTime = workingTime
        self.current.workingTime = workingTime
        self.status = "busy"
    
    def update(self, timediff):
        if self.current:
            if self.workingTime <= 0:
                self.current.status = "done"
                item = self.current
                self.current = None
                self.status = "idle"
                
                return item
                
            self.workingTime -= timediff
            

class Model:
    """ Simple queueing system without fails
    
    - Examples:
    ```
    >> model = Model()
    >> model.start()
    ```
    """
    def __init__(
        self, 
        queueDistribution: str = "uniform",
        queueDistributionValue: tuple = (0, 1),
        queueIntensity: str = "auto",
        queueIntensityValue: float = 1.0,
        queueType: str = "LIFO",
        
        agentsDistribution: str = "uniform",
        agentsDistributionValue: tuple = (0, 1),
        agentsIntensity: str = "auto",
        agentsIntensityValue: float = 1.0,
        agentsCount: int = 1,
        
        measure: str = "sec", 
        endtime: str = "static",
        endtimeValue: float = 1.0
    ):
        self.queueDistribution = queueDistribution
        self.queueDistributionValue = queueDistributionValue
        self.queueIntensity = queueIntensity
        self.queueIntensityValue = queueIntensityValue
        self.queueType = queueType
        
        self.agentsDistribution = agentsDistribution
        self.agentsDistributionValue = agentsDistributionValue
        self.agentsIntensity = agentsIntensity
        self.agentsIntensityValue = agentsIntensityValue
        self.agentsCount = agentsCount
        
        self.measure = measure
        self.endtime = endtime
        self.endtimeValue = endtimeValue
        
        self.queue = []
        self.queueMaxLength = 0
        self.nextItemTime = 0
        self.nextItemTimeCurrent = 0
        self.out = []
        
        self.agents = []
        
        self.elapsed = 0
        self.loadFactor = 0
        self.interrupt = False
    
    
    def getMeasure(self) -> float:
        """ Get model measure
        
        - Returns:
            measure value: float
        """
        measure = {
            "sec": 1,
            "min": 60,
            "hour": 60 * 60
        }
        
        return measure[self.measure]
        
        
    def arrivalTime(self) -> float:
        """ Get item arrival time 
        
        - Returns:
            static value or value from distribution: float
        """
        if self.queueIntensity == "static":
            return self.getQueueTime()
        
        else:
            return distribution(self.queueDistribution, self.queueDistributionValue) * self.getMeasure()
    
    
    def serviceTime(self) -> float:
        """ Get service time 
        
        - Returns:
            static value or value from distribution: float
        """
        if self.agentsIntensity == "static":
            return self.getServiceTime()
        
        else:
            return distribution(self.agentsDistribution, self.agentsDistributionValue) * self.getMeasure()
    

    def getQueueTime(self) -> float:
        """ Get time for next item
        t_q = \frac{1}{\lambda} \cdot scale
        
        - Returns:
            static value or value from distribution: float
        """
        if self.queueIntensity == "static":
            return 1 / self.queueIntensityValue * self.getMeasure()
        
        else:
            return self.nextItemTime
        
    
    def getServiceTime(self) -> float:
        """ Get time for service
        $t_a = \frac{1}{\lambda} \cdot scale$
        
        - Returns:
            static value or value from distribution: float
        """
        if self.agentsIntensity == "static":
            return 1 / self.agentsIntensityValue * self.getMeasure()
        
        else:
            return self.serviceTime()
    
    
    def getQueueIntensity(self) -> float:
        """ Get queue intensity
        $\lambda = \frac{1}{t_q} \cdot scale
        
        - Returns:
            static value or value from distribution: float
        """
        if self.queueIntensity == "static":
            return self.queueIntensityValue
            
        else:
            return 1 / self.nextItemTime * self.getMeasure()
 
    
    def getAgentsIntensity(self) -> float:
        """ Get agents intensity
        $\mu = \frac{ \sum_{n} \frac{1}{t_a} \cdot scale }{n}
        
        - Returns:
            static value or value from distribution: float
        """
        if self.agentsIntensity == "static":
            return self.agentsIntensityValue
        
        else:
            return sum([ 
                self.getMeasure() / agent.current.workingTime if agent.current else 0 for agent in self.agents 
            ]) / len(self.agents)
    
    
    def getLoadFactor(self) -> float:
        """ Get model load factor
        \rho = \frac{\lambda}{\mu}
        
        - Returns:
            value: float
        """
        if self.getAgentsIntensity():
            self.loadFactor = self.getQueueIntensity() / self.getAgentsIntensity()
        
        return self.loadFactor
    
    
    def getQueueAverageLength(self) -> float:
        """ Get average queue length
        
        - Returns:
            value: float
        """
        return len(self.queue) / len(self.agents)
    
    
    def printStatistics(self) -> None:
        """ Prints statistics to stdout 
        """
        stats = {
            "elapsed time":         f"{ round(self.elapsed, 1) } s",
            "arrival time":         f"{ round(self.nextItemTimeCurrent) } s",
            "queue length":         len(self.queue),
            "queue max length":     self.queueMaxLength,
            "queue average length": round(self.getQueueAverageLength(), 2),
            "queue intensity":      f"{ round(self.getQueueIntensity(), 2) } per { self.measure }",
            "agents intensity":     f"{ round(self.getAgentsIntensity(), 2) } per { self.measure }",
            "load factor":          round(self.getLoadFactor(), 2),
            "done":                 len(self.out)
        }
                                    
        for agent in self.agents:
            qiid = agent.current.id if agent.status == "busy" else "None"
            wtime = agent.workingTime if agent.status == "busy" else 0
            
            stats[f"agent: { agent.id }"] = f"\n\tstatus: { agent.status }\n\tqueue item id: { qiid }\n\tworking time: { round(wtime, 1) } s"
            
        output = []
        
        for (k, v) in stats.items():
            output.append(f"{ k }:\t{ v }")
        
        print("\n".join(output))
        
        
    def start(self, widgetOutput = None) -> None:
        """ Starts model calculation
        
        - Parameters:
            widgetOutput: optional    IPython widget Ouput
        """
        stime = time.monotonic()
        elapsedold, timediff = 0, 0
        id = 0
        
        # Initialize agents
        for n in range(self.agentsCount):
            self.agents.append(Agent(n))
        
        
        # Start simulation
        while self.elapsed < self.endtimeValue if self.endtime == "static" else True and not self.interrupt:
            
            # Check arrival time and add new item to queue if ready
            if self.nextItemTimeCurrent <= 0:
                self.nextItemTimeCurrent = self.arrivalTime()
                self.nextItemTime = self.nextItemTimeCurrent
                id += 1
                self.queue.append(QueueItem(id))
            
            # Register max queue length
            if len(self.queue) > self.queueMaxLength:
                self.queueMaxLength = len(self.queue)
            
            # Check all agents and give new item if status is idle
            for agent in self.agents:
                if len(self.queue) and agent.status == "idle":
                    # Check queue type
                    qIndex = -1 if self.queueType == "LIFO" else 0
                    item = self.queue.pop(qIndex)
                    
                    agent.perform(item, self.serviceTime())
                
                # Update agents state
                itemDone = agent.update(timediff)
                
                # Collect "done" items
                if itemDone:
                    self.out.append(itemDone)
            
            # Update queue state
            for item in self.queue:
                item.update(timediff)
            
            # Get elapsed time and time step
            elapsedold = self.elapsed
            self.elapsed = time.monotonic() - stime
            timediff = self.elapsed - elapsedold
            
            # Update current arrival time
            self.nextItemTimeCurrent -= timediff
            
            # Print statistics to stdout or widget output
            if widgetOutput:
                with widgetOutput:
                    clear_output(wait = True)
                    self.printStatistics()
            
            else:
                clear_output(wait = True)
                self.printStatistics()