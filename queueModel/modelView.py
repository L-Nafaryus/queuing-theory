from IPython.display import clear_output, display
import ipywidgets as widgets
from multiprocessing import Process


class ModelView:
    def __init__(self, model):
        self.model = model
        self.view = self.createView()
        self.instance = None
        self.proc = None
    

    def apply(self, settings):
        def onClick(_):
            self.update(settings)
        
        return onClick
    
    
    def run(self, view):
        def onClick(_):
            view.children[0].children[2].value = "Busy"
            
            self.proc = Process(target = self.instance.start, args = (view.children[1], ))
            #self.proc.daemon = True
            self.proc.start()
            self.proc.join()
            
            view.children[0].children[2].value = "Idle"
        
        return onClick
    
    
    def stop(self, view):
        def onClick(_):
            #if view.children[0].children[2].value == "Busy":
            if self.instance:
                self.instance.interrupt = True
        
        return onClick
    
    
    def changeDistribution(self, dropboxParent):
        def onChange(_):
            values = {
                "static": "1",
                "uniform": "(0, 1)",
                "poisson": "1",
                "normal": "(0, 1)"
            }

            dropboxParent.children[1].value = values[dropboxParent.children[0].value]

        return onChange

    
    def changeIntensity(self, intensityParent):
        def onChange(_):
            values = {
                "static": "1",
                "auto": "-1"
            }
            value = values[intensityParent.children[0].value]

            if value ==  "":
                intensityParent.children[1].value = value
                intensityParent.children[1].disabled = True

            else:
                intensityParent.children[1].value = value
                intensityParent.children[1].disabled = False

        return onChange
    
    
    def createView(self):
        ###
        #    Settings
        ##
        queueSettings = widgets.VBox([
            widgets.Label("Queue"),
            widgets.HBox([
                widgets.Dropdown(
                    description = "Distribution",
                    options = [ "static", "uniform", "poisson", "normal" ],
                    value = "uniform"
                ),
                widgets.Text(
                    value = "(0, 1)"
                )
            ]),
            widgets.HBox([
                widgets.Dropdown(
                    description = "Intensity",
                    options = [ "static", "auto" ],
                    value = "auto" 
                ),
                widgets.Text(
                    value = "-1",
                    disabled = True
                )
            ]),
            widgets.Dropdown(
                description = "Type",
                options = [ "FIFO", "LIFO" ],
                value = "FIFO" 
            )
        ])
        
        queueSettings.children[1].children[0].observe(
            self.changeDistribution(queueSettings.children[1])
        )
        queueSettings.children[2].children[0].observe(
            self.changeIntensity(queueSettings.children[2])
        )
        
        agentsSettings = widgets.VBox([
            widgets.Label("Agents"),
            widgets.HBox([
                widgets.Dropdown(
                    description = "Distribution",
                    options = [ "static", "uniform", "poisson", "normal" ],
                    value = "uniform" 
                ),
                widgets.Text(
                    value = "(0, 1)"
                )
            ]),
            widgets.HBox([
                widgets.Dropdown(
                    description = "Intensity",
                    options = [ "static", "auto" ],
                    value = "auto" 
                ), 
                widgets.Text(
                    value = "-1",
                    disabled = True
                )
            ]),
            widgets.Text(
                description = "Count",
                value = "1"
            )
        ])
        
        agentsSettings.children[1].children[0].observe(
            self.changeDistribution(agentsSettings.children[1])
        )
        agentsSettings.children[2].children[0].observe(
            self.changeIntensity(agentsSettings.children[2])
        )
        
        generalSettings = widgets.VBox([
            widgets.Label("General"),
            widgets.Dropdown(
                description = "Measure",
                options = [ "sec", "min", "hour" ],
                value = "sec" 
            ),
            widgets.HBox([
                widgets.Dropdown(
                    description = "End time",
                    options = [ "static", "auto" ],
                    value = "static" 
                ),
                widgets.Text(
                    value = "1",
                    disabled = False
                )
            ]),
            widgets.Button(
                description = "Apply"
            )
        ])
        
        generalSettings.children[2].children[0].observe(
            self.changeIntensity(generalSettings.children[2])
        )
        
        settings = widgets.VBox([
            queueSettings, 
            agentsSettings,
            generalSettings
        ])
        
        generalSettings.children[3].on_click(self.apply(settings))
        
        ###
        #    View
        ##
        view = widgets.VBox([
            widgets.HBox([
                widgets.Button(
                    description = "Run"
                ),
                widgets.Button(
                    description = "Stop",
                    disabled = False
                ),
                widgets.Text(
                    description = "Status:",
                    value = "Idle",
                    disabled = True
                )
            ]),
            widgets.Output()
        ])
        
        view.children[0].children[0].on_click(self.run(view))
        view.children[0].children[1].on_click(self.stop(view))
        
        ###
        #    Tab
        ##
        tab = widgets.Tab([settings, view])
        tab.set_title(0, "Settings")
        tab.set_title(1, "View")
        
        return tab

        
    def update(self, settings):
        totuple = lambda text: tuple(
            map(float, text.replace(" ", "").replace("(", "").replace(")", "").split(","))
        )
        queue = settings.children[0]
        agents = settings.children[1]
        general = settings.children[2]
        
        self.instance = self.model(
            queueDistribution = queue.children[1].children[0].value, 
            queueDistributionValue = totuple(queue.children[1].children[1].value),
            queueIntensity = queue.children[2].children[0].value,
            queueIntensityValue = float(queue.children[2].children[1].value),
            queueType = queue.children[3].value, 
            
            agentsDistribution = agents.children[1].children[0].value,
            agentsDistributionValue = totuple(queue.children[1].children[1].value),
            agentsIntensity = queue.children[2].children[0].value,
            agentsIntensityValue = float(queue.children[2].children[1].value),
            agentsCount = int(agents.children[3].value), 
            
            measure = general.children[1].value,
            endtime = general.children[2].children[0].value,
            endtimeValue = float(general.children[2].children[1].value)
        )

        
    def display(self):
        display(self.view)
        
        ###
        #    Initialize model
        ##
        self.update(self.view.children[0])