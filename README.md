# queuing-theory

Данный репозиторий носит обучающий характер по теории массового обслуживания. 

## Содержание
- ![Ряд контрольных заданий и тестирование модели](task.ipynb)
- ![Модель запросов](queueModel)

## Зависимости

Текущие версии:
- Python 3.9.4
- JupyterLab 3.0.14

Установка:
```bash
$ pip install requirements.txt
$ jupyter nbextension install --py widgetsnbextension
$ jupyter nbextension enable --py widgetsnbextension
```

Запуск:
```bash
$ jupyter-lab task.ipynb
```

## Предупреждение
- В текущей версии отсутствует возможность остановки цикла работы модели, поэтому не стоит задавать опцию бесконечного времени симуляции.

## Лицензия
[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
